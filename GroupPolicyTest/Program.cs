﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupPolicyTest
{
    class Program
    {
        static void Main(string[] args) {
            // See https://aka.ms/new-console-template for more information
            Console.WriteLine("test group policy management");

            removeSettingScreen(); //복구와 시간 설정 제거 
            //recoverSettingScreen();

            //시간 설정은 제어판에도 존재 하여 같이 제거해야함.
            removeTimeCpl(); //제어판의 시간 설정 제거 
            try
            {
                //recoverTimeCpl(); //recover는 해당 레지스트리를 삭제 하는 것으로 없는 것을 삭제 요청시 exception 발생 
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());  
            }


            //GPedit snap-in 접근허용 제어 
            restricUseGpEdit("0"); //mode 1: 사용제한 , 0:사용제한 없음

            //레지스트리 편집 권한 제어 
            try {
                restrictRegistryEdit(null); //mode 1: 사용제한 , null: 사용제한 없음 
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            
            
        }

        //mode 1: 사용제한 , null(delete): 사용제한 없음 
        static private void restrictRegistryEdit(string mode) {
            ComputerGroupPolicyObject.SetPolicySetting(@"HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\System!DisableRegistryTools", mode, RegistryValueKind.DWord);
        }

        //mode 1: 사용제한 , 0:사용제한 없음
        static private void restricUseGpEdit(string mode) {
            ComputerGroupPolicyObject.SetPolicySetting(@"HKCU\Software\Policies\Microsoft\MMC\{8FC0B734-A0E1-11D1-A7D3-0000F87571E3}!Restrict_Run", mode, RegistryValueKind.DWord);
        }
        
        static private void removeTimeCpl() {
            ComputerGroupPolicyObject.SetPolicySetting(@"HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer!DisallowCpl", "1", RegistryValueKind.DWord);
            ComputerGroupPolicyObject.SetPolicySetting(@"HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\DisallowCpl!1", "Microsoft.DateAndTime", RegistryValueKind.String);
        }

        static private void recoverTimeCpl() {
            ComputerGroupPolicyObject.SetPolicySetting(@"HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer!DisallowCpl", null, RegistryValueKind.DWord);
            ComputerGroupPolicyObject.SetPolicySetting(@"HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\DisallowCpl!1", null, RegistryValueKind.String);
        }
        static private void removeSettingScreen() {
            
            //object rtnObj = ComputerGroupPolicyObject.GetPolicySetting(@"HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer!SettingsPageVisibility");
            //Console.WriteLine("" + rtnObj);
            //reference :: https://learn.microsoft.com/ko-kr/windows/uwp/launch-resume/launch-settings-app#update-and-security 
            ComputerGroupPolicyObject.SetPolicySetting(@"HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer!SettingsPageVisibility", "hide:recovery;dateandtime", RegistryValueKind.String );

        }

        static private void recoverSettingScreen() {
            ComputerGroupPolicyObject.SetPolicySetting(@"HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer!SettingsPageVisibility", "", RegistryValueKind.String);
        }
    }
}

